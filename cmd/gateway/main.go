package main

import (
	"context"
	"log"
	"net/http"
	"os"

	"github.com/getsentry/sentry-go"
	"github.com/gorilla/websocket"
	"github.com/juju/errors"
	kafka "github.com/segmentio/kafka-go"
)

const (
	KAFKA_TOPIC        string = "raw-events"
	KAFKA_READER_GROUP string = "event-gateway-group"
	ADDR                      = "0.0.0.0:1212"
	MAX_CONNECTIONS           = 100
)

var upgrader = websocket.Upgrader{
	// Allow cross-domain connections
	CheckOrigin: func(r *http.Request) bool { return true },
}
var connections = make(map[*websocket.Conn]bool, MAX_CONNECTIONS * 2)

func main() {
	initSentry()
	go eventProcessor()
	http.HandleFunc("/api/event-gateway", eventGateway)
	log.Printf("Starting websocket server on %s\n", ADDR)
	log.Fatal(http.ListenAndServe(ADDR, nil))
}

func eventProcessor() {
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  []string{os.Getenv("KAFKA_HOST")},
		GroupID:  KAFKA_READER_GROUP,
		Topic:    KAFKA_TOPIC,
		MaxBytes: 1e6, // 10MB
	})

	ctx := context.Background()
	for {
		m, err := r.FetchMessage(ctx)
		if err != nil {
			logException(errors.Errorf("Failed to read kafka message: %w", err))
			continue
		}

		for conn := range connections {
			err := conn.WriteMessage(websocket.TextMessage, m.Value)
			if err != nil {
				logException(errors.Errorf("Failed to write event message: %w", err))
			}
		}

		err = r.CommitMessages(ctx, m)
		if err != nil {
			logException(errors.Errorf("Failed to commit kafka read: %w", err))
		}
	}
}

func eventGateway(w http.ResponseWriter, r *http.Request) {
    if len(connections) >= MAX_CONNECTIONS {
        http.Error(w, "The websocket gateway is at maximum capacity", http.StatusTooManyRequests)
        return
    }
	connection, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		logException(errors.Errorf("Failed to upgrade connection: %w", err))
		return
	}

	connections[connection] = true

	defer func() {
		delete(connections, connection)
		connection.Close()
	}()

	for {
		_, _, err := connection.ReadMessage()
		if err != nil {
			log.Println("Failed to read message:", err)
			break
		}
	}
}

func logException(err error) {
	sentry.CaptureException(err)
	log.Println(err)
}

func initSentry() {
	// SENTRY_DSN and SENTRY_ENVIRONMENT must be defined in env
	if err := sentry.Init(sentry.ClientOptions{}); err != nil {
		log.Printf("Sentry initialization failed: %v\n", err)
	}
}
