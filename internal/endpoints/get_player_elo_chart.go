package endpoints

import (
	"fmt"
	"github.com/juju/errors"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
)

type getPlayerEloChart struct {
	db *sqlx.DB
}

type GetPlayerEloChartParams struct {
	Name string `path:"name"`
}

type PlayerEloChart struct {
	Data []*DataPoint `json:"data"`
}

type DataPoint struct {
	Time  string `json:"time"`
	Value int    `json:"value"`
}

func (ep *getPlayerEloChart) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB

	ctx.Fizz.GET("/api/players/:name/elo-chart", []fizz.OperationOption{
		fizz.Summary("Get player mmr chart"),
		fizz.ID("get_player_elo_chart"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getPlayerEloChart) handle(c *gin.Context, params *GetPlayerEloChartParams) (*PlayerEloChart, error) {
	data, err := ep.fetchEloChart(params.Name)

	if err != nil {
		return nil, errors.Trace(err)
	}

	return &PlayerEloChart{Data: data}, nil
}

func (ep *getPlayerEloChart) fetchEloChart(name string) ([]*DataPoint, error) {
	data := make([]*DataPoint, 0)

	query := `
select
    sum(if (winner_name = :name, points_awarded, -points_lost)) as points,
	DATE(e.time) as day
from elo_transaction_1v1 as t
join events as e on e.event_id = t.event_id
where winner_name = :name or loser_name = :name
group by DATE(e.time)
order by day asc;`
	rows, err := ep.db.NamedQuery(query, map[string]interface{}{"name": name})

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	currentElo := 1000

	for rows.Next() {
		var points int
		var day string

		err = rows.Scan(&points, &day)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		currentElo += points

		data = append(data, &DataPoint{Time: day, Value: currentElo})
	}

	return data, nil
}
