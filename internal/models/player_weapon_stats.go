package models

type PlayerWeaponStats struct {
	PlayerName string       `json:"player_name"`
	Weapons    []WeaponStat `json:"weapons"`
}

type WeaponStat struct {
	Weapon           string   `json:"weapon"`
	WeaponName       string   `json:"weapon_name"`
	Usages           int      `json:"usages"`
	AverageItemPower float64  `json:"average_item_power"`
	KillFame         int      `json:"kill_fame"`
	DeathFame        int      `json:"death_fame"`
	Kills            int      `json:"kills"`
	Deaths           int      `json:"deaths"`
	Assists          int      `json:"assists"`
	FameRatio        *float64 `json:"fame_ratio"`
	WinRate          float64  `json:"win_rate"`
}
